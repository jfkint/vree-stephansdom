﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RouteTags : MonoBehaviour {

	public bool RouteA;
	public bool RouteB;
	public bool RouteC;

	[Serializable]
	public class StringBoolDictionary : SerializableDictionary<string, bool> {}
	
	
	public StringBoolDictionary routes = new StringBoolDictionary();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
