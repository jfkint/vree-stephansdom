﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class RouteManager : MonoBehaviour {
	
	public enum RoutesEnum {
		RouteA, RouteB, RouteC
	}

//	public RoutesEnum selectedRoute = RoutesEnum.RouteA;

	public List<string> RouteNames = RouteNameStorage.RouteNames;

	[Header("Create new Route")]
	public string RouteName = "New Random Route";
	[Range(0.0f, 100.0f)]
	public float RandomProbability = 50.0f;
	
//	[Header("Select Route")]
	
	private RouteTags[] _routeTagElements;

	[NonSerialized]
	private string _selectedRoute;

	// Use this for initialization
	void Start () {
		_routeTagElements = GetComponentsInChildren<RouteTags>();
		foreach (RouteTags tagsElement in _routeTagElements) {
			// If the object is not rendered on start, disable it completely
//			if (!tagsElement.gameObject.GetComponent<MeshRenderer>().enabled)
//				tagsElement.gameObject.SetActive(false);
		}
	}
	
	// Update is called once per frame
	void Update () {

		for (int i = 1; i <= 9; i++) {
			if (Input.GetKeyDown(i.ToString())) {
				Debug.Log(i + " key was pressed");
				if (RouteNames.Count >= i) {
					string selectedRoute = RouteNames[i - 1]; 
					Debug.Log("Displaying Route: '" + selectedRoute + "'");
					
					DisplayRoute(selectedRoute);
				} else {
					Debug.Log("No Route on Position " + (i-1));
				}
			}
		}

		if (Input.GetKeyDown("0")) {
			Debug.Log("0 key was pressed");
			Debug.Log("Displaying all Grips");
			DisplayAll();
		}


	}

//	public void DisplayRoute(RoutesEnum routeName) {
	public void DisplayRoute(string routeName) {
		_routeTagElements = GetComponentsInChildren<RouteTags>();

		foreach (RouteTags tagsElement in _routeTagElements) {
//			bool hideElement = (routeName == RoutesEnum.RouteA && !tagsElement.RouteA) ||
//			                   (routeName == RoutesEnum.RouteB && !tagsElement.RouteB) ||
//			                   (routeName == RoutesEnum.RouteC && !tagsElement.RouteC);
//			bool hideElement = tagsElement.routes[routeName];

//			tagsElement.gameObject.GetComponent<MeshRenderer>().enabled = !hideElement;
			tagsElement.gameObject.GetComponent<MeshRenderer>().enabled = tagsElement.routes[routeName];
		}
	}

	public void DisplayAll() {
		_routeTagElements = GetComponentsInChildren<RouteTags>();
		foreach (RouteTags tagsElement in _routeTagElements) {
			tagsElement.gameObject.GetComponent<MeshRenderer>().enabled = true;
		}
	}

	public void CreateRandomRoute() {
		
		float probability = RandomProbability / 100;
		string routeName = RouteName;
		
		if (!RouteNames.Contains(routeName)) {
			RouteNames.Add(routeName);				
		}

		RouteTags[] allGrips = GetComponentsInChildren<RouteTags>();

		foreach (RouteTags tagsElement in allGrips) {

			bool enable = Random.value <= probability;

			if (tagsElement.routes.ContainsKey(routeName)) {
				tagsElement.routes[routeName] = enable;
			} else {
				tagsElement.routes.Add(routeName, enable);
			}

		}
		
		
		DisplayRoute(routeName);
		
	}

	public void SelectRoute(string routeName) {
		DisplayRoute(routeName);
	}

	public void DeleteRoute(string selectedRoute) {
		RouteNames.Remove(selectedRoute);
		
		RouteTags[] allGrips = GetComponentsInChildren<RouteTags>();

		foreach (RouteTags tagsElement in allGrips) {

			if (tagsElement.routes.ContainsKey(selectedRoute)) {
				tagsElement.routes.Remove(selectedRoute);
			}
		}
		
		Debug.Log("Deleted Route: " + selectedRoute);

	}
}
