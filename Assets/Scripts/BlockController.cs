﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour {
    private Transform[] blocks = new Transform[28];
    
    //default values will change
    [Header("Resizing:")]
    [SerializeField]
    private float blockHeight;      //default: 0.257
    [SerializeField]
    private float blockWidth;       //default: 3.175
    [SerializeField]
    private float blockDepth;       //default: 0.3

    [Header("Repositioning:")]
    [SerializeField]
    private Vector3 origin;         //default: (0, 0.411, 0)
    [SerializeField]
    private float offset;           //default: 0.008

    // Use this for initialization
    void Start () {
        getBlocks();
        resizeBlocks(blockWidth, blockHeight, blockDepth);
        positionBlocks(origin, offset);
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void getBlocks()
    {
        GameObject blockContainer = GameObject.Find("Blocks");
        for (int i=0; i<28; i++)
        {
            blocks[i] = blockContainer.transform.GetChild(i);
        }
    }

    private void resizeBlocks(float _x, float _y, float _z)
    {
        for (int i=0; i<blocks.Length; i++)
        {
            blocks[i].transform.localScale = new Vector3(_x, _y, _z);
        }
    }

    private void positionBlocks(Vector3 origin, float offset)
    {
        //reset positions
        for (int i=0; i<blocks.Length; i++)
        {
            blocks[i].transform.localPosition = origin;
        }
        
        //reposition
        for (int i=1; i<blocks.Length; i++)
        {
            blocks[i].transform.localPosition = new Vector3(blocks[i - 1].transform.localPosition.x,
                                                            blocks[i - 1].transform.localScale.y + blocks[i - 1].transform.localPosition.y + offset,
                                                            blocks[i - 1].transform.localPosition.z);
        }
    }

    

}
