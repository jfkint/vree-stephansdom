﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GripController : MonoBehaviour {
    private Transform[] grips = new Transform[238];
    private Transform[] blocks = new Transform[28];

    //defult values will change
    [Header("Configuration:")]
    [SerializeField]
    public float offset;        // default: 0.28f
    [SerializeField]
    public float distance;      // default: 0.3f

    [Header("Resizing:")]
    [SerializeField]
    public float percentage;    // default: 10

    // Use this for initialization
    void Start () {
        getGrips();
        resizeGrips(percentage);
        printGripsOnBlocks(offset, distance);

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void printGripsOnBlocks(float rightOffset, float distance)     
    {
        getBlocks();
        int oddBlocks = 1;
        int evenBlocks = 0;
        Transform[,] oddBlockGrips = formOddGripMatrix();
        Transform[,] evenBlockGrips = formEvenGripMatrix();
        
        //Blocks with odd numbers
        for (int row=0; row<14; row++)
        {
            float blockSizeX = blocks[0].transform.localScale.x;
            float blockDepthZ = blocks[0].transform.localScale.z;
            
            for (int column=0; column<9; column++)
            {
                //oddBlockGrips[row, column].transform.parent = blocks[oddBlocks].transform;
                if (column == 0)
                {
                    oddBlockGrips[row, column].transform.localPosition = new Vector3(blocks[oddBlocks].transform.localPosition.x-(blockSizeX/2) + rightOffset,
                                                                                     blocks[oddBlocks].transform.localPosition.y,
                                                                                     blocks[oddBlocks].transform.localPosition.z+(blockDepthZ/2));
                } else {
                    oddBlockGrips[row, column].transform.localPosition = new Vector3(oddBlockGrips[row, column-1].transform.localPosition.x + distance,
                                                                                     blocks[oddBlocks].transform.localPosition.y,
                                                                                     blocks[oddBlocks].transform.localPosition.z+(blockDepthZ/2));
                }
            }
            oddBlocks += 2;
        }

        //Blocks with even numbers
        for (int row = 0; row < 14; row++)
        {
            float blockSizeX = blocks[0].transform.localScale.x;
            float blockDepthZ = blocks[0].transform.localScale.z;

            for (int column = 0; column < 8; column++)
            {
                //evenBlockGrips[row, column].transform.parent = blocks[evenBlocks].transform;
                if (column == 0)
                {
                    evenBlockGrips[row, column].transform.localPosition = new Vector3(blocks[evenBlocks].transform.localPosition.x - (blockSizeX / 2) + rightOffset + distance/2,
                                                                                     blocks[evenBlocks].transform.localPosition.y,
                                                                                     blocks[evenBlocks].transform.localPosition.z + (blockDepthZ / 2));
                } else {
                    evenBlockGrips[row, column].transform.localPosition = new Vector3(evenBlockGrips[row, column - 1].transform.localPosition.x + distance,
                                                                                     blocks[evenBlocks].transform.localPosition.y,
                                                                                     blocks[evenBlocks].transform.localPosition.z + (blockDepthZ / 2));
                }
            }
            evenBlocks += 2;
        }

        /*
        oddBlocks = 1;
        evenBlocks = 0;
        //Assign grips to blocks
        
        
        for (int row = 0; row < 14; row++)
        {
            for (int column = 0; column < 8; column++)
            {
                evenBlockGrips[row, column].transform.parent = blocks[evenBlocks].transform;
                //evenBlockGrips[row, column].transform.position = blocks[evenBlocks].transform.position;
            }
            evenBlocks += 2;
        }
        for (int row = 0; row < 14; row++)
        {
            for (int column = 0; column < 9; column++)
            {
                oddBlockGrips[row, column].transform.parent = blocks[oddBlocks].transform;
                //oddBlockGrips[row, column].transform.position = blocks[oddBlocks].transform.position;
            }
            oddBlocks += 2;
        }
        */
        
    }

    private Transform[,] formOddGripMatrix()
    {
        Transform[,] oddBlockGrips = new Transform[14, 9] { {rSG("flat04-1"), rSG("flat04-2"), rSG("flat07-1"), rSG("flat08-1"), rSG("flat01-1"), rSG("flat08-2"), rSG("flat20-1"), rSG("flat04-3"), rSG("flat04-4")},
                                                            {rSG("flat10-1"), rSG("flat03-1"), rSG("flat08-3"), rSG("flat12-1"), rSG("flat01-2"), rSG("flat08-4"), rSG("flat08-5"), rSG("flat12-2"), rSG("flat04-5")},
                                                            {rSG("flat02-1"), rSG("flat07-2"), rSG("flat03-2"), rSG("flat01-3"), rSG("flat12-3"), rSG("flat02-2"), rSG("flat12-4"), rSG("flat14-1"), rSG("flat02-3")},
                                                            {rSG("flat10-2"), rSG("flat04-6"), rSG("flat01-4"), rSG("flat12-5"), rSG("flat05-1"), rSG("flat11-1"), rSG("flat12-6"), rSG("flat05-2"), rSG("flat03-3")},
                                                            {rSG("flat09-1"), rSG("flat02-4"), rSG("flat20-2"), rSG("flat14-2"), rSG("flat05-3"), rSG("flat20-3"), rSG("flat02-5"), rSG("flat11-2"), rSG("flat20-4")},
                                                            {rSG("flat04-7"), rSG("flat07-3"), rSG("flat08-6"), rSG("flat02-6"), rSG("flat20-5"), rSG("flat12-7"), rSG("flat04-8"), rSG("flat08-7"), rSG("flat04-9")},
                                                            {rSG("flat10-3"), rSG("flat14-3"), rSG("flat10-4"), rSG("flat08-8"), rSG("flat07-4"), rSG("flat03-4"), rSG("flat14-4"), rSG("flat05-4"), rSG("flat03-5")},
                                                            {rSG("flat03-6"), rSG("flat04-10"), rSG("flat05-5"), rSG("flat11-3"), rSG("flat02-7"), rSG("flat14-5"), rSG("flat02-8"), rSG("flat07-5"), rSG("flat03-7")},
                                                            {rSG("flat10-5"), rSG("flat07-6"), rSG("flat02-9"), rSG("flat01-5"), rSG("flat08-9"), rSG("flat11-4"), rSG("flat20-6"), rSG("flat08-10"), rSG("flat20-7")},
                                                            {rSG("flat20-8"), rSG("flat04-11"), rSG("flat12-8"), rSG("flat14-6"), rSG("flat20-9"), rSG("flat03-8"), rSG("flat01-6"), rSG("flat20-10"), rSG("flat04-12")},
                                                            {rSG("flat03-9"), rSG("flat07-7"), rSG("flat11-5"), rSG("flat01-7"), rSG("flat07-8"), rSG("flat08-11"), rSG("flat05-6"), rSG("flat10-6"), rSG("flat03-10")},
                                                            {rSG("flat04-13"), rSG("flat08-12"), rSG("flat09-2"), rSG("flat20-11"), rSG("flat09-3"), rSG("flat02-10"), rSG("flat12-9"), rSG("flat05-7"), rSG("flat11-6")},
                                                            {rSG("flat08-13"), rSG("flat07-9"), rSG("flat04-14"), rSG("flat09-4"), rSG("flat14-7"), rSG("flat07-10"), rSG("flat12-10"), rSG("flat05-8"), rSG("flat03-11")},
                                                            {rSG("flat03-12"), rSG("flat07-11"), rSG("flat01-8"), rSG("flat20-12"), rSG("flat12-11"), rSG("flat09-5"), rSG("flat12-12"), rSG("flat20-13"), rSG("flat04-15")}
                                                         };

        return oddBlockGrips;
    }

    private Transform[,] formEvenGripMatrix()
    {
        Transform[,] evenBlockGrips = new Transform[14, 8] {{rSG("flat08-14"), rSG("flat14-8"), rSG("flat41-1"), rSG("flat31-1"), rSG("flat26-1"), rSG("flat42-1"), rSG("flat04-16"), rSG("flat03-13")},
                                                            {rSG("flat08-15"), rSG("flat12-13"), rSG("flat53-1"), rSG("flat48-1"), rSG("flat24-1"), rSG("flat36-1"), rSG("flat09-6"), rSG("flat03-14")},
                                                            {rSG("flat11-7"), rSG("flat02-11"), rSG("flat49-1"), rSG("flat43-1"), rSG("flat32-1"), rSG("flat39-1"), rSG("flat08-16"), rSG("flat04-17")},
                                                            {rSG("flat10-7"), rSG("flat11-8"), rSG("flat21-1"), rSG("flat46-1"), rSG("flat21-2"), rSG("flat40-1"), rSG("flat01-9"), rSG("flat05-9")},
                                                            {rSG("flat03-15"), rSG("flat20-14"), rSG("flat23-1"), rSG("flat29-1"), rSG("flat34-1"), rSG("flat25-1"), rSG("flat07-12"), rSG("flat04-18")},
                                                            {rSG("flat11-9"), rSG("flat14-9"), rSG("flat42-2"), rSG("flat44-1"), rSG("flat41-2"), rSG("flat38-1"), rSG("flat07-13"), rSG("flat02-12")},
                                                            {rSG("flat12-14"), rSG("flat03-16"), rSG("flat47-1"), rSG("flat26-2"), rSG("flat34-2"), rSG("flat25-2"), rSG("flat12-15"), rSG("flat02-13")},
                                                            {rSG("flat03-17"), rSG("flat08-17"), rSG("flat22-1"), rSG("flat27-1"), rSG("flat35-1"), rSG("flat43-2"), rSG("flat07-14"), rSG("flat10-8")},
                                                            {rSG("flat54-1"), rSG("flat20-15"), rSG("flat35-2"), rSG("flat45-1"), rSG("flat39-2"), rSG("flat23-2"), rSG("flat07-15"), rSG("flat24-2")},
                                                            {rSG("flat02-14"), rSG("flat02-15"), rSG("flat38-2"), rSG("flat31-2"), rSG("flat38-3"), rSG("flat57-1"), rSG("flat09-7"), rSG("flat12-16")},
                                                            {rSG("flat12-17"), rSG("flat03-18"), rSG("flat29-2"), rSG("flat44-2"), rSG("flat51-1"), rSG("flat45-2"), rSG("flat07-16"), rSG("flat09-8")},
                                                            {rSG("flat20-16"), rSG("flat02-16"), rSG("flat33-1"), rSG("flat22-2"), rSG("flat46-2"), rSG("flat27-2"), rSG("flat07-17"), rSG("flat20-17")},
                                                            {rSG("flat10-9"), rSG("flat07-18"), rSG("flat47-2"), rSG("flat50-1"), rSG("flat55-1"), rSG("flat50-2"), rSG("flat20-18"), rSG("flat02-17")},
                                                            {rSG("flat08-18"), rSG("flat09-9"), rSG("flat37-1"), rSG("flat33-2"), rSG("flat36-2"), rSG("flat51-2"), rSG("flat12-18"), rSG("flat02-18")}
                                                    };

        return evenBlockGrips;
    }

    private void getBlocks()
    {
        GameObject blockContainer = GameObject.Find("Blocks");
        for (int i = 0; i < 28; i++)
        {
            blocks[i] = blockContainer.transform.GetChild(i);
        }
    }

    private void getGrips()
    {
        //GameObject gripContainer = GameObject.Find("Grips");
        for (int i = 0; i < 238; i++)
        {
            grips[i] = transform.GetChild(i);
        }
    }

    private void resizeGrips(float percentage)
    {
        foreach (Transform grip in grips)
        {
            grip.transform.localScale = new Vector3(grip.transform.localScale.x * percentage/100,
                                                    grip.transform.localScale.y * percentage/100,
                                                    grip.transform.localScale.z * percentage/100);
        }
    }

    private Transform rSG(string gripName)    //return specific Grip
    {
        GameObject grip = new GameObject();
        for (int i = 0; i < grips.Length; i++)
        {
            grip = GameObject.Find(gripName);
        }
        return grip.transform;
    }
}
