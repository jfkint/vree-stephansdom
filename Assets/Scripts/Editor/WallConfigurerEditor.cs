﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(WallConfigurer))]
public class WallConfigurerEditor : Editor {

	
	
	public override void OnInspectorGUI() {
		DrawDefaultInspector();

		WallConfigurer myScript = (WallConfigurer) target;
		if (GUILayout.Button("Apply Configuration")) {
//			myScript.ApplyConfig();
			myScript.BuildWall();
		}
	}
}

