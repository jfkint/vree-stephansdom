﻿using UnityEditor;

[CustomPropertyDrawer(typeof(RouteTags.StringBoolDictionary))]
public class StringBoolPropertyDrawer : SerializableDictionaryPropertyDrawer {}