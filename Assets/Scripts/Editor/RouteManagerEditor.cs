﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(RouteManager))]
public class RouteManagerEditor : Editor {
	
//	private RouteManager.RoutesEnum _selectedRoute;
	private string _selectedRoute;

	private RouteManager _manager;

	private int _selectedIndex = 0;
	private float _chance = 50.0f;

	public override void OnInspectorGUI() {
		DrawDefaultInspector();
		
		_manager = (RouteManager) target;

		
		if (GUILayout.Button("Create Random Route")) {

			_manager.CreateRandomRoute();
			_selectedRoute = _manager.RouteName;
		}
		
		EditorGUILayout.Space();
		
		EditorGUILayout.LabelField("Select Route", EditorStyles.boldLabel);
		
		int oldIndex = _selectedIndex;
		_selectedIndex = EditorGUILayout.Popup(_selectedIndex, _manager.RouteNames.ToArray());
		
		if (_selectedIndex != oldIndex) {
			string newSelection = _manager.RouteNames[_selectedIndex];
			
			_selectedRoute = newSelection;
			Debug.Log("Route change: " + _selectedRoute);
			
			_manager.DisplayRoute(_selectedRoute);
		}
		
		if (GUILayout.Button("Delete Route")) {
			_manager.DeleteRoute(_selectedRoute);
			
			if (_manager.RouteNames.Count > 0) {
				_selectedRoute = _manager.RouteNames[0];
				_manager.DisplayRoute(_selectedRoute);
			} else {
				_manager.DisplayAll();
			}
		}
		
		EditorGUILayout.Space();
		
		if (GUILayout.Button("Display all Grips")) {
			_manager.DisplayAll();
		}

		if (GUILayout.Button("Add RouteTags Component to grips")) {
			AddRouteTagsToChildren();
		}
		
//		_chance = EditorGUILayout.Slider("Chance of visibility", _chance, 0.0f, 100.0f);
//		float probability = _chance / 100;
		
		

	}
	
	
	private void AddRouteTagsToChildren() {
		
		Transform[] allChildren = _manager.GetComponentsInChildren<Transform>();
		foreach (Transform child in allChildren) {

			if (child.gameObject.name.StartsWith("flat")) {
				if (child.gameObject.GetComponent<RouteTags>() == null) {
					child.gameObject.AddComponent<RouteTags>();
				}
			
//				child.gameObject.SetActive(false);
			}
			
			
		}
	}
}
