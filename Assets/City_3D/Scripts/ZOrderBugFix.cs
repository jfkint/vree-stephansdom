﻿using UnityEngine;
using System.Collections;

public class ZOrderBugFix : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Material mat = GetComponent<Renderer> ().material;
		mat.SetInt ("_ZWrite", 1);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
