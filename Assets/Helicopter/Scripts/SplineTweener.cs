﻿using System;
using UnityEngine;
using Pixelplacement;
using UnityEngine.Events;

public class SplineTweener : MonoBehaviour
{
	public Spline mySpline;
	public Transform myObject;
	public float duration;

	public int startTimeSec = 10;

	private AudioSource _audioSource;

	private Action endCallback;

	void Awake ()
	{
//		Tween.Spline (mySpline, myObject, 0, 1, true, duration, 0, Tween.EaseInOut, Tween.LoopType.PingPong);
	}

	public void Start() {
		_audioSource = GetComponent<AudioSource>();
		Invoke("StartHelicopter", startTimeSec);
	}

	public void StartHelicopter() {

		endCallback += stopHelicopter;
		
		Tween.Spline (mySpline, myObject, 0, 1, true, duration, 0, Tween.EaseInOut, Tween.LoopType.None, null, endCallback);
		_audioSource.Play();
	}

	private void stopHelicopter() {
		_audioSource.Stop();
		Debug.Log("stopped audio");
	}
}